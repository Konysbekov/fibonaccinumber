package com.example.fibonaccicalculator

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.fibonaccicalculator.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var isCalculating = false
    private val handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.startButton.setOnClickListener {
            if (!isCalculating) {
                startCalculation()
            } else {
                cancelCalculation()
            }
        }
    }

    private fun startCalculation() {
        isCalculating = true
        binding.startButton.text = getString(R.string.cancel)
        binding.inputNumber.isEnabled = false
        binding.currentText.visibility = View.VISIBLE
        binding.resultText.visibility = View.GONE

        val userNumber = binding.inputNumber.text.toString().toInt()
        val n = userNumber + 2  // Shift by two positions
        Thread(FibonacciRunnable(n, userNumber)).start()
    }

    private fun cancelCalculation() {
        isCalculating = false
        binding.startButton.text = getString(R.string.start)
        binding.inputNumber.isEnabled = true
        binding.resultText.text = ""
        binding.resultText.visibility = View.GONE
        binding.currentText.visibility = View.GONE
        handler.removeCallbacksAndMessages(null)
    }

    private inner class FibonacciRunnable(private val n: Int, private val userNumber: Int) : Runnable {
        override fun run() {
            val fib = LongArray(n)
            if (n > 0) fib[0] = 0
            if (n > 1) fib[1] = 1

            for (i in 2 until n) {
                if (!isCalculating) return
                fib[i] = fib[i - 1] + fib[i - 2] // This follows the rule F(n) = F(n-1) + F(n-2)
            }

            for (i in 0 until userNumber) {
                if (!isCalculating) return
                val currentIndex = i + 1  // Current order (index)
                handler.post {
                    binding.currentText.text = "Current is $currentIndex"
                    android.util.Log.d("FibonacciRunnable", "Updated currentText with: $currentIndex")
                }
                try {
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }

            handler.post {
                isCalculating = false
                binding.startButton.text = getString(R.string.start)
                binding.inputNumber.isEnabled = true
                binding.currentText.visibility = View.GONE
                binding.resultText.text = "Result is: ${fib[userNumber]}"
                binding.resultText.visibility = View.VISIBLE
                android.util.Log.d("FibonacciRunnable", "Calculation completed with result: ${fib[userNumber]}")
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("isCalculating", isCalculating)
        outState.putString("currentText", binding.currentText.text.toString())
        outState.putString("resultText", binding.resultText.text.toString())
        outState.putInt("currentTextVisibility", binding.currentText.visibility)
        outState.putInt("resultTextVisibility", binding.resultText.visibility)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        isCalculating = savedInstanceState.getBoolean("isCalculating")
        binding.currentText.text = savedInstanceState.getString("currentText")
        binding.resultText.text = savedInstanceState.getString("resultText")
        binding.currentText.visibility = savedInstanceState.getInt("currentTextVisibility")
        binding.resultText.visibility = savedInstanceState.getInt("resultTextVisibility")
        if (isCalculating) {
            binding.startButton.text = getString(R.string.cancel)
            binding.inputNumber.isEnabled = false
        }
    }
}
